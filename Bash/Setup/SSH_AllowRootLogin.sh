#!/bin/sh

###################################
# Change History:
# 2019-01-20:
#   > Uploaded copy to Repo "Public-Scripts"
###################################

sed -i.bak "s@\n#PermitRootLogin prohibit-password@##PermitRootLogin prohibit-password\nPermitRootLogin yes@g" /etc/ssh/sshd_config
service sshd restart