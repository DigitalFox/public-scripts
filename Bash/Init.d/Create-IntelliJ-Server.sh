#! /bin/sh

###################################
# Change History:
# 2019-01-20:
#   > Reformat spacing
#   > Uploaded copy to Repo "Public-Scripts"
###################################

scriptFolder=~/scripts
scriptName=run_IntelliJServer.sh
scriptNameNoExtension=run_IntelliJServer
mkdir -p ${scriptFolder}


cat << 'EOF' > ${scriptFolder}/${scriptName}
#! /bin/bash

### BEGIN INIT INFO
# Provides:          run_IntelliJServer
# Required-Start:    $local_fs $network
# Required-Stop:     $local_fs
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: Starts the IntelliJIDEALicenseServer on boot-up
# Description:       Starts the IntelliJIDEALicenseServer on boot-up (port 1017)
### END INIT INFO

# >> This should be located in /etc/init.d/<script name>

# >> Source: https://gist.github.com/drmalex07/298ab26c06ecf401f66c
# https://gist.github.com/naholyr/4275302


# Carry out specific functions when asked to by the system
case "$1" in
  start)
    echo "Starting IntelliJ server..."
    echo "A $(date) - run_IntelliJServer" >> /root/scripts/output.txt
    sleep 10
    sudo -u root bash -c '~/IntelliJIDEALicenseServer_linux_amd64 &'
    echo "B $(date) - run_IntelliJServer" >> /root/scripts/output.txt
    ;;
  stop)
    echo "Trying to find PID of IntelliJ..."
    pid=$(netstat -plant | grep IntelliJ | awk -F '[ /]+' '{print $7}')
    if [ -z "$pid" ]; then  echo "Either IntelliJ not already running, or cannot get PID"; else kill -15 $pid; fi
    sleep 1
    ;;
  *)
    echo "Usage: /etc/init.d/run_IntelliJServer {start|stop}"
    exit 1
    ;;
esac

exit 0
EOF

chmod +x ${scriptFolder}/${scriptName}

# Flipping this around so when I do "ls -la" I can remember where this goes...
# ln -s ${scriptFolder}/${scriptName} /etc/init.d/${scriptName}

mv ${scriptFolder}/${scriptName} /etc/init.d/${scriptNameNoExtension}
ln -s /etc/init.d/${scriptNameNoExtension} ${scriptFolder}/${scriptName}
update-rc.d ${scriptNameNoExtension} defaults